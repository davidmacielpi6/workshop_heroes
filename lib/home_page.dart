import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'result_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String _search;
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    Future<List> searchHeroes() async {
      var response = await http.get(
        "https://superheroapi.com/api/253549809252402/search/$_search",
      );

      return json.decode(response.body)["results"];
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          "HEROES",
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      ),
      backgroundColor: Color.fromRGBO(59, 66, 84, 1),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(30),
            child: TextField(
              style: TextStyle(
                color: Colors.white,
              ),
              decoration: InputDecoration(
                labelText: "Nome",
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(
                    style: BorderStyle.solid,
                    width: 1,
                  ),
                ),
              ),
              onChanged: (value) {
                _search = value;
              },
            ),
          ),
          _isLoading
              ? CircularProgressIndicator()
              : FlatButton(
                  onPressed: () async {
                    setState(() {
                      _isLoading = true;
                    });
                    FocusScope.of(context).unfocus();
                    var result = await searchHeroes();
                    setState(() {
                      _isLoading = false;
                    });

                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => ResultsPage(
                          results: result,
                          searchName: _search,
                        ),
                      ),
                    );
                  },
                  child: Text(
                    "Buscar",
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  color: Color.fromRGBO(66, 76, 94, 1),
                ),
        ],
      ),
    );
  }
}
