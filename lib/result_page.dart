import 'package:flutter/material.dart';

import 'detail_page.dart';

class ResultsPage extends StatelessWidget {
  final String searchName;
  final List<dynamic> results;

  const ResultsPage({Key key, this.results, this.searchName}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(66, 76, 94, 1),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text(
          searchName != null ? searchName : "",
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      body: results == null
          ? Center(
              child: Text(
                "NÃO FOI POSSIVEL ENCONTRAR UM HEROI",
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            )
          : ListView.builder(
              padding: EdgeInsets.all(10),
              itemCount: results.length,
              itemBuilder: (context, index) => Card(
                elevation: 10,
                color: Color.fromRGBO(66, 76, 94, 1),
                child: ListTile(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => DetailPage(
                          imageUrl: results[index]["image"]["url"],
                        ),
                      ),
                    );
                  },
                  contentPadding: EdgeInsets.all(5),
                  leading: Image.network(
                    results[index]["image"]["url"],
                  ),
                  title: Text(
                    results[index]["name"],
                    style: TextStyle(color: Colors.white),
                  ),
                  subtitle: Text(
                    results[index]["biography"]["full-name"],
                    style: TextStyle(color: Colors.white),
                  ),
                  trailing: Icon(
                    Icons.navigate_next,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
    );
  }
}
